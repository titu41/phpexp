<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
          
<?php if($message = Session::get('success')): ?>
           
                 <strong><?php echo e($message); ?></strong>
           
           <?php endif; ?>
            <div class="content">
               <table style="width:100%" border="1">
                   <thead>
                       <th>Name</th>
                       <th>Original Name</th>
                       <th>Extension</th>
                       <th>Size</th>
                       <th>Uplaoded Time</th>
                       <th>Action</th>
                   </thead>
                   <tbody>
                       <?php $__currentLoopData = $files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <tr>
                           <td><?php echo e($file->file_name); ?></td>
                           <td><?php echo e($file->original_name); ?></td>
                           <td><?php echo e($file->ext); ?></td>
                           <td><?php echo e($file->size); ?></td>
                           <td><?php echo e($file->created_at); ?></td>
                           <td><a href="<?php echo e(asset('api/delete_file')); ?>/<?php echo e($file->id); ?>">Remove</a></td>
                       </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </tbody>
               </table>
            </div>
        </div>
    </body>
</html>
