<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\User;
use App\MediaFile;
class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:cronjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Media file manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
     

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hourdiff = round((strtotime($time1) - strtotime($time2))/3600, 1);
        if($hourdiff > 4)
        {
            $file = MediaFile::find($request->media_id);
            unlink('public/uploads/'.$file->file_name);

            $delete = $file->delete();
        }
    }
    
     
    
}
