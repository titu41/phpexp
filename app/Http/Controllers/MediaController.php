<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\MediaFile;


class MediaController extends Controller
{
   
    public function saveFile(Request $request)
    {
         if($request->isMethod('post')) 
         {
            $media = new MediaFile;
            
            if ($request->hasFile('file')) {
                
                $file_size = $_FILES['file']['size'];
                
                $image = $request->file('file');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
               
                
                $media->file_name = $name;
                $media->original_name = $image->getClientOriginalName();
                $media->size = $file_size;
                $media->ext = $image->getClientOriginalExtension();
               
                $insert = $media->save();
                
                if($insert)
                     return redirect('api/file_list')->with('success', 'The file has been uploaded successfully');
                else
                    return redirect('api/file_list')->with('success', 'The file has not been uploaded');
                    
           }
             
         }   
    }
    
    public function fileList(Request $request)
    {

      $files = MediaFile::orderBy('created_at', 'desc')->get();
      
      
      return view('file_list', compact('files')); 
      
    }
    
    public function deleteFile(Request $request)
    {

      $file = MediaFile::find($request->media_id);
      unlink('public/uploads/'.$file->file_name);
      
      $delete = $file->delete();
      
      if($delete)
           return redirect('api/file_list')->with('success', 'The file has been deleted successfully');
      else
          return redirect('api/file_list')->with('success', 'The file has not been deleted');
      
    }
    
}
