<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('upload_file', function() {
    return view('file_upload');
});
Route::post('save_file','MediaController@saveFile' );
Route::get('file_list','MediaController@fileList' );
Route::get('delete_file/{media_id}','MediaController@deleteFile' );
